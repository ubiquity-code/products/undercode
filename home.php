<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Ubiquity_Undercode
 */
get_header();

	if ( have_posts() ) : ?>

		<header>
			<h1 class="page-title"><?php echo single_post_title( '', false ) ?: 'Posts'; ?></h1>
		</header>

		<div class="row">

			<div class="col-12 col-md-9">

				<?php /* Start the Loop */
				while ( have_posts() ) :
					the_post();

					/*
						* Include the Post-Type-specific template for the content.
						* If you want to override this in a child theme, then include a file
						* called content-___.php (where ___ is the Post Type name) and that will be used instead.
						*/
					get_template_part( 'inc/content', get_post_type() );

				endwhile;

				the_posts_navigation(); ?>

			</div>
			<div class="col-12 col-md-3">
				<?php get_sidebar(); ?>
			</div>
			
		</div><?php
	else :

		get_template_part( 'inc/content', 'none' );

	endif;

get_footer();
