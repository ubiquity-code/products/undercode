<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Ubiquity_Undercode
 */

// Standard header from UCDF
ucdf_header();

get_template_part( 'inc/content', 'header' );