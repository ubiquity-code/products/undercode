#!/usr/bin/env bash
# filename: child.sh


##
## 1. Set up our variables
##

# Move to this script's location first of all (so the relative paths don't put the child directory within the parent template)
cd "$(dirname "$0")"

# Set up stateful prompt colours & messages
COLOUR_INFO='\033[1;36m'
COLOUR_WARN='\033[1;33m'
COLOUR_DEFAULT='\033[0m'
COLOUR_SUCCESS='\033[1;32m'
ANSWER_YES_OR_NO="Please answer Yes (Y/y) or No (N/n)"

echo -e "${COLOUR_INFO}ℹ️  NOTE: This script has only succeeded if you see the party emoji!${COLOUR_DEFAULT}"

# Get current directory name
PARENT_DIR=$(basename $(pwd))
PARENT_PATH="../$PARENT_DIR"
echo -e "\n${COLOUR_INFO}ℹ️  Parent directory is $PARENT_DIR"

# Child theme directory name
CHILD_PATH="../$PARENT_DIR-child"
CHILD_DIR=$(basename $CHILD_PATH)

##
## 2. Check for child theme directory
##

if [ -d $CHILD_PATH ]; then
	# Child directory already exists - ask user to confirm overwrite before proceeding
	echo -e "\n${COLOUR_WARN}⚠️  Child directory already exists. If you proceed, your current Undercode child theme ($CHILD_DIR) will be removed. Do you wish to proceed (y/n)?${COLOUR_DEFAULT}"
	while true; do
		read -p "" yn
		case $yn in
			[Yy]* ) rm -rf $CHILD_PATH; break;;
			[Nn]* ) exit;;
			* ) echo -e "\n${COLOUR_WARN}⚠️  $ANSWER_YES_OR_NO ${COLOUR_DEFAULT}";;
		esac
	done
fi

##
## 3. Create child theme directory
##

# Make child directory
echo -e "\n${COLOUR_INFO}ℹ️  Creating child theme directory: $CHILD_DIR"
mkdir $CHILD_PATH
echo -e "\n${COLOUR_SUCCESS}✅ Child theme directory '$CHILD_DIR' created"

##
## 4. Copy parent files to child theme
##

# Copy relevant files over
echo -e "\n${COLOUR_INFO}ℹ️  Copying files for child theme"
cp screenshot.png $CHILD_PATH
cp .gitignore $CHILD_PATH

# Use 'ditto' on OS X, otherwise use cp --parents
DITTO_TYPE=$( type ditto ) > /dev/null
if [ "$DITTO_TYPE" ]; then
	echo -e "\n${COLOUR_INFO}ℹ️  Using ditto command to copy"
	CP_COMMAND=ditto
else
	echo -e "\n${COLOUR_INFO}ℹ️  Using cp command to copy"
	CP_COMMAND="cp -R --parents"
fi
$CP_COMMAND ./child-files/ $CHILD_PATH
$CP_COMMAND ./webpack.mix.js $CHILD_PATH
$CP_COMMAND ./src "$CHILD_PATH/src"


echo -e "\n${COLOUR_SUCCESS}✅ Files copied"

##
## 5. Update child theme's files
##

# Rename any .sample files to their main equivalent
# Sample files exist to, for example, avoid package.json hijacking script runs in the child-files dir of the parent theme
echo -e "\n${COLOUR_INFO}ℹ️  Moving to child theme's directory"
cd $CHILD_PATH

echo -e "\n${COLOUR_INFO}ℹ️  Renaming sample files"
for f in *.sample; do
	mv -- $f ${f%.sample}
done
echo -e "\n${COLOUR_SUCCESS}✅ Sample files renamed"

# Child theme needs different info in the style.css header comment - we'll update that now to save the user the effort
echo -e "\n${COLOUR_INFO}ℹ️  Updating child theme's WordPress header comment"
CHILD_STYLE_FILE="$CHILD_PATH/src/scss/_style-header.scss"
CHILD_TEMP_FILE="$CHILD_STYLE_FILE.tmp"

while read LINE; do

	# Trim the line (no need for leading spaces)
	TRIMMED_LINE=${LINE##*( )}
	FIRST_TWO=${TRIMMED_LINE:0:2}
	
	if [ "$TRIMMED_LINE" == "" ]; then
		# If we reach an empty line we're done with key-value pairs. Close out the comment and leave
		echo "*/" >> $CHILD_TEMP_FILE
		break;
	else
		
		# Set Internal field separator
		IFS=":"; read -a fields <<< "$TRIMMED_LINE";
		
		# Get the key-value pairs.
		# NOTE: For some reason, VAL contains a leading space. Be aware!
		for i in "${!fields[@]}"; do
			if [ $i -eq 0 ]; then
				KEY=${fields[$i]}
				VAL=""
			else
				VAL="$VAL${fields[$i]}"
			fi
		done

		# Use the uniqueness of the opening comment to pick our spot for inserting new lines
		if [ "$FIRST_TWO" == "/*" ]; then
			echo "$KEY" >> $CHILD_TEMP_FILE;
			echo "Template: $PARENT_DIR" >> $CHILD_TEMP_FILE
		else
			# In the normal case, just update anything for this key and append
			case $KEY in
				"Theme Name" )
					VAL="$VAL Child";
					;;

				Description )
					VAL=" A child theme of$VAL"
					;;

				"Text Domain" )
					VAL="${VAL}c"
					;;
			esac

			# Output the updated key-value pair line to the temp file
			echo "$KEY:$VAL" >> $CHILD_TEMP_FILE
		fi

		# If we've reached the end of the comment block, break out (no need to read the entire file)
		if [ "$FIRST_TWO" == "*/" ]; then
			echo "$TRIMMED_LINE" >> $CHILD_TEMP_FILE
			break;
		fi;
	fi

done < $CHILD_STYLE_FILE

# Overwrite original style header with our constructed one
mv -- $CHILD_TEMP_FILE ${CHILD_TEMP_FILE%.tmp} 
echo -e "\n${COLOUR_SUCCESS}✅ Header comment updated"

cd $PARENT_PATH
echo -e "\n${COLOUR_INFO}ℹ️  We're back in the parent theme"

##
## 6. Initialise the child theme
##

echo -e "\n${COLOUR_INFO}ℹ️  The child theme needs to be initialised, including building src assets, in order to work properly. You can manually do this by running this command in the child theme's directory:${COLOUR_DEFAULT}"
echo -e "\n\t npm install && npm run dev"
echo -e "\n${COLOUR_INFO}However, I can run this for you now. ${COLOUR_WARN}Would you like me to do that? (y/n)${COLOUR_DEFAULT}"
while true; do
	read -p "" yn
	case $yn in
		[Yy]* )
			# Initialise the theme at the user's request
			echo -e "\n${COLOUR_INFO}ℹ️  Moving to child directory"
			cd $CHILD_PATH

			echo -e "\n${COLOUR_INFO}ℹ️  Initialising theme"
			npm install

			echo -e "\n${COLOUR_INFO}ℹ️  Building src assets${COLOUR_DEFAULT}"
			npm run dev

			echo -e "\n${COLOUR_INFO}ℹ️  Returning to parent directory"
			cd $PARENT_PATH

			# If WP-CLI is installed, we can make the child theme active at the user's request
			WP_CLI_TYPE=$( type wp ) > /dev/null
			if [ "$WP_CLI_TYPE" ]; then
				echo -e "\n${COLOUR_INFO}ℹ️  I've noticed you have WP-CLI installed, so I can activate your new child theme immediately. Would you like me to do that? (y/n)${COLOUR_DEFAULT}"
				while true; do
					read -p "" yn
					case $yn in
						[Yy]* )
							# Activate the theme if we need to (wp theme is-active returns exit code 0 for true and 1 for false). We use the || IS_ACTIVE part to avoid our "set -e" context
							# bombing us out on a non-0 exit code
							IS_ACTIVE=0
							wp theme is-active "$CHILD_DIR" >> /dev/null || IS_ACTIVE=$?
							
							if [ $IS_ACTIVE -eq 0 ]; then
								echo -e "\n${COLOUR_INFO}ℹ️  It looks like this new child theme is already set as the active theme at the moment (possibly as the result of a previous install). I don't need to do anything here${COLOUR_DEFAULT}"
							else
								wp theme activate "$CHILD_DIR"
							fi
							break
							;;
						[Nn]* )
							break
							;;
						* ) echo -e "\n${COLOUR_WARN}⚠️  $ANSWER_YES_OR_NO ${COLOUR_DEFAULT}";;
					esac
				done
			fi
			break
			;;

		[Nn]* ) 
			break
			;;

		* )
			echo -e "\n${COLOUR_WARN}⚠️  $ANSWER_YES_OR_NO ${COLOUR_DEFAULT}"
			;;
	esac
done

echo -e "\n${COLOUR_SUCCESS}✅ Child theme created successfully! 🎉${COLOUR_DEFAULT}"

if [ ! -d "$CHILD_PATH/node_modules" ]; then
	echo -e "\n${COLOUR_DEFAULT} Remember to initialise your theme before using it, by running: "
	echo -e "\n\t npm install"
fi
if [ ! -f "$CHILD_PATH/style.css" ]; then
	echo -e "\n${COLOUR_DEFAULT} Remember to build your src assets before using the theme, by running: "
	echo -e "\n\t npm run dev"
fi