<?php
/**
 * Ubiquity Undercode functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Ubiquity_Undercode
 */


if ( ! defined( 'UC_VERSION' ) ) {
	/**
	 * Constant: Current version of the theme
	 * 
	 * @since 0.1.0
	 * 
	 * @var string
	 */
	define( 'UC_VERSION', wp_get_theme()->version );
}

if( !defined( 'UC_TEXT_DOMAIN' ) ){
	/**
	 * Constant: Text domain for the theme
	 * 
	 * @since 0.1.0
	 * 
	 * @var string
	 */
	define( 'UC_TEXT_DOMAIN', 'uc' );
}

if( !defined( 'UC_FRONT_SECTIONS' ) ){
	/**
	 * Constant: Whether to enable "front sections". If enabled, any
	 * pages created with the front page as their parent will instead
	 * be used as sections on the front page, including direct access
	 * to them being disabled
	 * 
	 * @since 0.1.0
	 * 
	 * @var bool
	 */
	define( 'UC_FRONT_SECTIONS', true );
}

if( !defined( 'UC_SLUG' ) ){
	define( 'UC_SLUG', 'undercode' );
}

if( !defined( 'UC_THEME_TRANSIENT_UPDATE_SLUG' ) ){
	define( 'UC_THEME_TRANSIENT_UPDATE_SLUG', 'uc_update_theme_' . UC_SLUG );
}

if( !defined( 'UC_HOST_URL' ) ){
	define( 'UC_HOST_URL', 'https://www.ubiquitycode.co.uk' );
}

if( !defined( 'UC_THEME_URL' ) ){
	define( 'UC_THEME_URL', sprintf( '%s/wp/themes/%s', UC_HOST_URL, UC_SLUG ) );
}

if( !function_exists( 'uc_ucdf_config' ) ){

	/**
	 * UCDF config
	 * 
	 * @since 0.1.0
	 * 
	 * @see ucdf_config
	 * 
	 * @param array $defaults Default config passed in from plugin
	 * 
	 * @return array The custom config
	 */
	function uc_ucdf_config( array $defaults ){

		return array(
			'fa_kit_id' => '5344320b6a'
		);

	}
	add_filter( 'ucdf_config', 'uc_ucdf_config' );

}

/**
 * Report any missing theme dependencies in the admin area
 * 
 * @since 0.1.0
 * 
 * @return void
 */
function uc_theme_dependencies() : void {
	if( !class_exists( 'UCDF' ) ){
		uc_admin_error( 'The Ubiquity Underscores theme requires the <a href="https://gitlab.com/ubiquity-code/products/wordpress-developer-framework" title="UCDF plugin repo" target="_blank">UCDF plugin</a> to function correctly. Please install and activate this plugin' );
	}
}
add_action( 'admin_notices', 'uc_theme_dependencies' );


if ( ! function_exists( 'uc_setup' ) ) :

	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 * 
	 * @since 0.1.0
	 */
	function uc_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 */
		load_theme_textdomain( UC_TEXT_DOMAIN, get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'main' => __( 'Main Navigation', UC_TEXT_DOMAIN ),
				'extra' => __( 'Extra Links', UC_TEXT_DOMAIN )
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'uc_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);	
	}
endif;
add_action( 'after_setup_theme', 'uc_setup' );

/**
 * Disable file editing for security
 * 
 * Emulates defining DISALLOW_FILE_EDIT, but since we can't (easily) inject the
 * declaration, we just remove the caps manually. Also hides the relevant admin
 * menu items
 * 
 * @since 0.1.0
 * 
 * @return void
 */
function uc_disable_file_editing() : void {

	foreach( wp_roles()->role_objects as $role ){
		$role->remove_cap( 'edit_themes' );
		$role->remove_cap( 'edit_plugins' );
		$role->remove_cap( 'edit_files' );
	}

	add_action( 'admin_menu', function(){
		remove_submenu_page( 'appearence', 'theme-editor' );
		remove_submenu_page( 'plugins', 'plugin-editor' );
	}, 110 );

}
add_action( 'init', 'uc_disable_file_editing' );


/**
 * Set the content width in pixels, based on the theme's design and stylesheet.a
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
/*function uc_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'uc_content_width', 640 );
}
add_action( 'after_setup_theme', 'uc_content_width', 0 );*/

/**
 * Register widget area.
 *
 * @since 0.1.0
 */
function uc_widgets_init() {
	/*register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', UC_TEXT_DOMAIN ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', UC_TEXT_DOMAIN ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);*/

	/**
	 * Widget area: '404 After'
	 * 
	 * Widget area for the 404 page after the image and search
	 * 
	 * @since 0.1.0
	 */
	register_sidebar(
		array(
			'name'			=> esc_html__( '404 After', UC_TEXT_DOMAIN ),
			'id'			=> '404',
			'description'	=> 'Placed on the 404 page after the image and search field',
			'before_widget'	=> '<section id="%1$s" class="widget %2$s">',
			'after_widget'	=> '</section>',
			'before_title'	=> '<h2 class="widget-title">',
			'after_title'	=> '</h2>'
		)
	);

	/**
	 * Widget area: Archive Sidebar
	 * 
	 * Widget area for the sidebar of archive pages
	 */
	register_sidebar(
		array(
			'before_title'	=> '<h2 class="widget-title subheading heading-size-3">',
			'after_title'	=> '</h2>',
			'before_widget'	=> '<div class="widget %2$s"><div class="widget-content">',
			'after_widget'	=> '</div></div>',
			'name'			=> __( 'Archive Sidebar', UC_TEXT_DOMAIN ),
			'id'			=> 'sidebar-archive',
			'description'	=> __( 'Widgets in this area will be displayed in the sidebar of archives.', 'twentytwenty' ),
		)
	);


}
add_action( 'widgets_init', 'uc_widgets_init' );

/**
 * Enqueue scripts and styles.
 * 
 * @since 0.1.0
 */
function uc_scripts() {
	//	Original _s assets
	// 		wp_enqueue_style( '_s-style', ucdf_theme_url( 'style_s.css' ), array(), UC_VERSION );
	// 		wp_style_add_data( '_s-style', 'rtl', 'replace' );
	// 		wp_enqueue_script( 'uc-navigation', get_template_directory_uri() . '/js/navigation.js', array(), UC_VERSION, true );

	ucdf_enqueue_style( 'uc-style', get_stylesheet_uri() );
	ucdf_enqueue_script( 'uc-script', get_theme_file_uri( 'js/main.js' ) );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'uc_scripts' );

/**
 * Include children of the current page.
 * 
 * Must be used inside The Loop. Utilises the 'content-page-child' included template
 * 
 * @since 0.1.0
 */
function uc_page_children(){

	$home_sections = get_children(array(
		'post_parent'	=> get_the_ID(),
		'post_type'		=> 'page',
		'post_status'	=> 'publish',
		'orderby'		=> 'menu_order',
		'order'			=> 'ASC'
	));

	foreach( $home_sections as $section ){
		$set = ucdf_set_the_post( $section );
		if( !is_wp_error( $set ) ){
			get_template_part( 'inc/content', 'page-child' );
		}else{
			ucdf_error( $set );
		}
	}

}

/**
 * Get the posts for front sections
 * 
 * @since 0.1.0
 * 
 * @see get_children
 * 
 * @return array|bool An
 */
function uc_front_sections(){

	$page_on_front = ( 'page' == get_option( 'show_on_front' ) );
	$front_ID = get_option( 'page_on_front' );

	if( !UC_FRONT_SECTIONS || !$page_on_front || !$front_ID ){
		return false;
	}

	return get_posts(array(
		'post_parent'	=> $front_ID,
		'post_type'		=> 'page',
		'post_status'	=> 'publish',
		'orderby'		=> 'menu_order',
		'order'			=> 'ASC'
	));
}

/**
 * Prevent access to certain pages.
 * 
 * Sets the 404 parameter of the global query early on to honour the
 * standard WordPress execution process
 * 
 * @since 0.1.0
 */
function uc_prevent_page_access(){
	global $wp_query;

	$is_404 = false;
	$object = $wp_query->get_queried_object();

	// Throw 404 if trying to directly access a home section
	if( UC_FRONT_SECTIONS && ( $object instanceof WP_Post ) ):
		
		$page_on_front		= ( 'page' == get_option( 'show_on_front' ) );
		$parent_on_front	= ( get_option( 'page_on_front' ) == $object->post_parent );		
		
		if( is_page() && $page_on_front && $parent_on_front ):
			$is_404 = true;
		endif;

	endif;

	/**
	 * Allow filtering of the 404 value to be overridden
	 * 
	 * @since 0.1.0
	 * 
	 * @param bool $is_404 The current value of whether the 404 should be set
	 * 
	 * @return bool The overridden value of whether the 404 should be set
	 */
	$is_404 = apply_filters( 'uc_404', $is_404 );

	if( $is_404 ){
		$wp_query->set_404();
	}

}
add_action( 'wp', 'uc_prevent_page_access' );


/**
 * Exclude certain items from search results
 * 
 * @see pre_get_posts
 * 
 * @param WP_Query $query @see pre_get_posts
 */
function uc_search_filter( WP_Query $query ){

	// Exclude front sections
	if( $query->is_search() ){

		$front_sections = uc_front_sections();

		if( $front_sections ){
		
			$query->set( 'post__not_in', array_map(function( $post ){
				return $post->ID;
			}, $front_sections ) );

		}
	}
}
add_action( 'pre_get_posts', 'uc_search_filter' );

/**
 * Handle SSL verifcation for requests
 * 
 * @since 0.1.0
 * 
 * @see https_ssl_verify
 * 
 * @param bool		$verify @see https_ssl_verify
 * @param string	$url	@see https_ssl_verify
 * 
 * @return bool Whether to verify the SSL certificate
 */
function uc_https_ssl_verify( $verify, $url ){

	$host	= parse_url( $url, PHP_URL_HOST );
	$parts	= explode( '.', $host );

	// Skip verification on local envs (to avoid SSL error 60)
	if( end( $parts ) == 'local' ){
		return false;
	}

	return $verify;
}
add_filter( 'https_ssl_verify', 'uc_https_ssl_verify', 10, 2 );

/**
 * Enable custom updates for the theme
 * 
 * @since 0.1.0
 * 
 * @see themes_api
 * 
 * @param false|object|array	$override	@see themes_api
 * @param string				$action		@see themes_api
 * @param array|object			$args		@see themes_api
 */
function uc_themes_api( $override, string $action, $args ){

	// Bail if not getting theme info, or not working on this theme
	if( ( 'theme_information' !== $action) || ( UC_SLUG !== $args->slug ) ){
		return false;
	}

	// Check the cache (get from remote if nothing exists)
	if( false == $remote = get_transient( UC_THEME_TRANSIENT_UPDATE_SLUG ) ){

		$remote = wp_remote_get( sprintf( '%s/info', UC_THEME_URL ), array(
			'timeout' => 10,
			'headers' => array(
				'Accept' => 'application/json'
			)
		));

		// Use the 12-hour cache if it's valid
		if( ucdf_remote_is_valid( $remote ) ){
			ucdf_set_update_transient( UC_THEME_TRANSIENT_UPDATE_SLUG, $remote );
		}

	}

	// Inject the right info if we can
	if( ucdf_remote_is_valid( $remote ) ){

		$result = json_decode( $remote['body'] );
		$res	= (object)array(
			'name'			=> $result->name,
			'slug'			=> $result->slug,
			'version'		=> $result->version,
			'tested'		=> $result->tested,
			'requires'		=> $result->requires,
			'author'		=> sprintf( '<a href="%s" title="Author\'s website">Ubiquity Code</a>', UC_HOST_URL ),
			'download_link'	=> $result->download_url,
			'trunk'			=> $result->download_url,
			'requires_php'	=> $result->requires_php,
			'last_updated'	=> $result->last_updated,
			'sections'		=> array(
				'description' 		=> $result->sections->description,
				'installation'		=> $result->sections->installation,
				'changelog'			=> $result->sections->changelog
			)
		);

		if( isset( $result->sections->screenshots ) ){
			$res->sections['screenshots'] = $result->sections->screenshots;
		}

		if( isset( $result->banners ) ){
			$res->banners = array(
				'low'	=> $result->banners->low,
				'high'	=> $result->banners->high
			);
		}

		if( isset( $result->versions ) ){
			$res->versions = $result->versions;
		}

		unset( $remote_is_valid );
		return $res;
	}

	// If we get here, we failed. TODO: AJAX-insert a notice
	unset( $remote_is_valid );
	return false;

}
add_filter( 'themes_api', 'uc_themes_api', 10, 3 );

/**
 * Push theme update info to WP transients
 * 
 * @since 0.1.0
 * 
 * @see site_transient_update_themes
 * 
 * @param object|array $transient @see site_transient_update_themes
 * 
 * @return object @see site_transient_update_themes
 */
function uc_site_transient_update_themes( $transient ){

	if( empty( $transient->checked ) ){
		return $transient;
	}

	// Try to use cache first
	if( false == $remote = get_transient( UC_THEME_TRANSIENT_UPDATE_SLUG ) ){
	
		$remote = wp_remote_get( sprintf( '%s/info', UC_THEME_URL ), array(
			'timeout' => 10,
			'headers' => array(
				'Accept' => 'application/json'
			)
		));

		if( ucdf_remote_is_valid( $remote ) ){
			ucdf_set_update_transient( UC_THEME_TRANSIENT_UPDATE_SLUG, $remote );
		}
		
	}

	if( $remote ){

		$result = json_decode( $remote['body'] );

		if( $result && version_compare( UC_VERSION, $result->version, '<' ) && version_compare( $result->requires, get_bloginfo( 'version' ), '<=' ) ){
			$res = array(
				'theme'        	=> UC_SLUG,
				'version'		=> UC_VERSION,
				'new_version'	=> $result->version,
				'tested'		=> $result->tested,
				'requires'		=> $result->requires,
				'package'		=> $result->download_url,
				'url'			=> $result->url
			);
			$transient->response[$res['theme']] = $res;
		}

		return $transient;

	}

}
add_filter( 'site_transient_update_themes', 'uc_site_transient_update_themes' );

/**
 * Clear cache after updating
 * 
 * @since 0.1.0
 */
add_action( 'upgrader_process_complete', function( $upgrader_object, $options ){
	if( ( 'update' == $options['action'] ) && ( 'theme' == $options['type'] ) ){
		delete_transient( UC_THEME_TRANSIENT_UPDATE_SLUG );
	}
}, 10, 2 );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/functions/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/functions/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/functions/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/functions/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/functions/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/functions/woocommerce.php';
}