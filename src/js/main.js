// Safe jQuery inclusion
$ = $ || jQuery;

// Bootstrap
import 'bootstrap';

let $topNav, $topStrip, $pageHeader, $pageFooter, $adminBar;

function adjustFooter(){

	// Remove whitespace below footer by putting the footing 
	if( ( parseInt( $pageFooter.css( 'margin-top' ), 10 ) + $pageFooter.offset().top + $pageFooter.height() ) < $( window ).height() ){
		let $adjustment = $pageFooter.outerHeight();

		if( $pageHeader.length ){
			$adjustment += $pageHeader.outerHeight();
		}
		if( $adminBar.length ){
			$adjustment += $adminBar.outerHeight();
		}
		
		$( 'main' ).css({
			'height': 'calc( 100vh - '+ $adjustment + 'px )',
		});
		$pageFooter.css({
			'position': 'fixed',
			'bottom': 0,
			'width': '100vw'
		});
	}

}

function checkTopNav(){
	let isStuck = false;

	if( !ucdfIsMobile() ){
		let navOffset	= 0;
		let affectedBy = [
			$topStrip,
			$adminBar
		];

		affectedBy.forEach( function( $el ){
			if( $el.is( ':visible' ) ){
				navOffset += $el.height();
			}
		});

		isStuck = ( $( window ).scrollTop() > navOffset );
	}

	$topNav.toggleClass( 'stuck', isStuck );
}

function pageSetup(){
	adjustFooter();
	checkTopNav();
}

// On page load
$(function(){
	$topStrip	= $( '.top-strip' );
	$topNav		= $( '.top-nav' );
	$pageHeader	= $( 'body > header' );
	$pageFooter = $( 'body > footer' );
	$adminBar	= $( '#wpadminbar' );

	pageSetup();

	$( '.wc_tabs[role="tablist"] a' ).on( 'click', function(){
		e.preventDefault()
		$(this).tab('show')
	})
});

// Page setup on load to let images load first
// TODO: Require jQuery 3.x and update these ancient handler calls
$(window).on('load', function(){
	pageSetup();

	$( window ).scroll(function(){
		pageSetup();
	});
	$( window ).resize(function(){
		pageSetup();
	});
});