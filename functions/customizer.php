<?php
/**
 * Ubiquity Undercode Theme Customizer
 *
 * @package Ubiquity_Undercode
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function uc_customise_register( $wp_customize ) {
	/*$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial(
			'blogname',
			array(
				//'selector'        => '.site-title a',
				'render_callback' => 'uc_customize_partial_blogname',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'blogdescription',
			array(
				//'selector'        => '.site-description',
				'render_callback' => 'uc_customize_partial_blogdescription',
			)
		);
	}*/

	$wp_customize->add_setting( 'uc_animated_header', array(
		'default'			=>	true,
		'sanitize_callback' => 'uc_sanitise_checkbox'
	));

}
add_action( 'customize_register', 'uc_customise_register' );

/**
 * Sanitise a checkbox
 * 
 * @since 0.1.0
 * 
 * @param mixed					$value		The value to sanitise
 * @param WP_Customize_Setting	$setting	The setting object
 */
function uc_sanitise_checkbox( $value, WP_Customize_Setting $setting ){
	return (bool)$value;
}

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function uc_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function uc_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function uc_customize_preview_js() {
	wp_enqueue_script( 'uc-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), UC_VERSION, true );
}
add_action( 'customize_preview_init', 'uc_customize_preview_js' );
