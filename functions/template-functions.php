<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package Ubiquity_Undercode
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function uc_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'uc_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function uc_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'uc_pingback_header' );

/**
 * Add an admin error
 * 
 * @since 0.1.0
 * 
 * @see uc_get_admin_error
 * 
 * @param string $text The text for the error
 * 
 * @return void
 */
function uc_admin_error( string $text ) : void {
	echo uc_get_admin_error( $text );
}
	/**
	 * Return markup for an admin error
	 * 
	 * @since 0.1.0
	 * 
	 * @param string $text The text for the error
	 * 
	 * @return string
	 */
	function uc_get_admin_error( string $text ) : string {
		return sprintf( '<div class="error"><p>%s</p></div>', $text );
	}