## Ubiquity Undercode

[[_TOC_]]

## Requirements
This theme requires the following to work:
- [npm](https://www.npmjs.com/) - a package manager
- [UCDF ](https://gitlab.com/ubiquity-code/products/wordpress-developer-framework) - our developer framework plugin

Note that if UCDF isn't active, this theme will add an error notice to your admin area to remind you to install and activate it

## Installation
1. Add this theme to WordPress in [the standard way](https://wordpress.org/support/article/using-themes/#adding-new-themes)
2. Run `npm install` in your theme directory

## Directory Structure

### functions/
Included function files (bootstrapped from main functions.php file)

### inc/
Template parts included by the theme

### js/
Compiled JS - **do not edit** ([see why](#compiling-css-js))

### languages/
All things i18n/L10n

**TODO:** Translations aren't in place by default, but the languages folder has been left in the theme so that the default pot file can be used should it be required

### src/
Source assets (SCSS & JS) - **do edit!**

## Compiling Source CSS & JS
This theme uses [Laravel Mix](https://laravel-mix.com/) as its source compiler (with a slightly different structure mentioned below) and includes Mix's [npm scripts](https://laravel-mix.com/docs/5.0/installation#npm-scripts). We advise that you run `npm watch` before making any changes to source assets so that Mix automatically validates them and re-compiles your assets.

## Source Structure
Javascript source files are located in `src/js` 

SCSS source files are located in `src/scss`. The main style file (`src/scss/style.scss`) includes partials from the `src/scss/inc` folder.

### WooCommerce Support
If your site will use WooCommerce, you can use the import of the `_woocommerce` partial. If not, then you can remove that import without damaging the rest of the site

### SCSS Partials

#### _bootstrap
Any [Bootstrap theming](https://getbootstrap.com/docs/4.5/getting-started/theming/) should be done here.

Imports the Bootstrap source SCSS libraries at the bottom of the file - the default configuration of these imports is the minimal amount the default theme needs to work. Feel free to comment out any of the optional list which you're sure aren't required for your site.

Bootstrap themselves may add/change the modular import structure - we don't guarantee such changes will be immediately reflected in this theme, but at the time of writing the most up-to-date list from Bootstrap will be located in the theme at `node_modules/bootstrap/scss/bootstrap.scss`

**Note:** Two important factors here:
1. Theming should all be handled *above* the Bootstrap source imports so that the imports use the correct variables
2. Removing colours from Bootstrap maps only works if placed after the required list (functions, variables and mixins) so that the maps aren't re-defined by Bootstrap

#### _fonts
Local fonts should be added here - there is even a helpful `@font-face` example in the file's comments

#### _footer
Additional styles for the footer

#### _front_sections
Additional styles for [Front Sections](#front-sections)

#### _mixins
Mixins used by the theme.

**Note:** Add new ones as you please, but be cautious about removing any in here by default as it may affect the theme's output

#### _nav
Additional styles relating to navigation menus

#### _ucdf
Additional styles for markup generated by the [UCDF plugin](https://gitlab.com/ubiquity-code/products/wordpress-developer-framework) 

#### _utilities
Additional utility classes used by the theme. Note as per [_mixins](#_mixins)

#### _variables
Additional variables used by the theme Note as per [_mixins](#_mixins)

#### _woocommerce
Additional styles for markup generated by the [WooCommerce plugin](https://woocommerce.com/)

#### _wp
Additional styles for markup generated by WordPress core, including default Gutenberg blocks

## Front Sections
There is a constant in the theme's `functions.php` file called `UC_FRONT_SECTIONS`. When enabled, this implements the concept of "front sections" on the site. Any pages created as children of the front page will display as sections on the front page instead of pages in their own right. Front sections are hidden from search results and cannot be accessed by URL (a 404 is displayed instead).

Front sections use the template `inc/content-page-child.php` by default.

## Authors
- James Cushing - _Creator_ - [Ubiquity Code](https://www.ubiquitycode.co.uk)

## Licence
This project is licensed under the GPL v2 licence - see the [LICENCE.md](LICENCE.md) file for details