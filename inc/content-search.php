<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Ubiquity_Undercode
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'card my-5' ); ?>>
	<header class="card-header entry-header">
		<?php the_title( sprintf( '<h2 class="entry-title m-0"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

		<?php if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php
			uc_posted_on();
			uc_posted_by();
			?>
		</div>
		<?php endif; ?>
	</header>

	<?php uc_post_thumbnail(); ?>

	<div class="card-body entry-summary">
		<?php the_excerpt(); ?>
	</div>

	<footer class="card-footer entry-footer">
		<?php uc_entry_footer(); ?>
	</footer>
</article>
