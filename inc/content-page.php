<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Ubiquity_Undercode
 */

?>

<article <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title text-center">', '</h1>' ); ?>
	</header>

	<?php uc_post_thumbnail(); ?>

	<div class="entry-content container-xl<?php if( is_front_page() ){ echo ' text-center'; } ?>">
		<?php
		the_content();

		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', UC_TEXT_DOMAIN ),
				'after'  => '</div>',
			)
		);
		?>
	</div>


	<?php if( false ):
		// Disabled. For now... ?>
		
		<?php if ( get_edit_post_link() ) : ?>
			<footer class="entry-footer">
				<?php
				edit_post_link(
					sprintf(
						wp_kses(
							/* translators: %s: Name of current post. Only visible to screen readers */
							__( 'Edit <span class="sr-only">%s</span>', UC_TEXT_DOMAIN ),
							array(
								'span' => array(
									'class' => array(),
								),
							)
						),
						wp_kses_post( get_the_title() )
					),
					'<span class="edit-link">',
					'</span>'
				);
				?>
			</footer>
		<?php endif; ?>
	<?php endif; ?>
</article>
