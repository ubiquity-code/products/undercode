<section class="post-<?php the_ID(); ?> page-child my-5 container-xl">
	<?php if( has_post_thumbnail() ): ?>
		<figure class="page-child-thumbnail">
			<img class="img-fluid" src="<?php the_post_thumbnail_url() ?>" title="Image">
		</figure>
	<?php endif; ?>
	<div class="page-child-content">
		<h2><?php the_title(); ?></h2>
		<?php the_content(); ?>

		<button class="btn btn-success">Download Now!</button>
	</div>
</section>