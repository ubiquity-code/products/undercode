<footer class="bg-secondary text-light mt-5 p-5">
	<div class="d-flex flex-column align-items-center"><?php
		ucdf_nav_menu(array(
			'theme_location'	=> 'extra',
			'menu_class'		=> 'd-flex flex-column align-items-center flex-md-row',
			'container'			=> '',
		));

		get_template_part( 'inc/content', 'footer-after' ); ?>
	</div>
</footer>