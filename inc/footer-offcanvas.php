<?php // d-md-none for not appearing on larger screens, d-none to be toggled by controller ?>
<nav id="menu-offcanvas" class="offcanvas bg-light d-none d-md-none">
	<?php ucdf_nav_menu(array(
		'theme_location'	=> 'main',
		'menu_class'		=> 'fixed-bottom d-flex flex-column justify-content-end align-items-center pb-5 mb-5',
		'container'			=> '',
	)) ?>
</nav>
