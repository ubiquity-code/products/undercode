<a class="skip-link sr-only" href="#primary"><?php esc_html_e( 'Skip to content', UC_TEXT_DOMAIN ); ?></a>

<?php // Top nav ?>
<header class="container-fluid md-sticky-top top-nav py-2 bg-white">
	<nav class="d-flex flex-column flex-center flex-lg-row justify-content-lg-between">
		<a class="navbar-brand d-flex flex-center m-0 my-md-2" href="/" title="Home">
			<img src="<?php ucdf_theme_logo(); ?>" title="Logo" />
		</a>
		
		<?php ucdf_nav_menu(array(
			'theme_location'	=> 'main',
			'menu_class'		=> 'align-items-center d-none d-md-flex',
			'container'			=> '',
		)) ?>
	</nav>
</header>
<main id="main" class="container-fluid pt-5">