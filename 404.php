<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Ubiquity_Undercode
 */

get_header(); ?>
	
	<section class="error-404">
		<header class="page-header">
			<h1 class="page-title"><small class="text-muted">404 <span class="text-light">|</span></small> <?php esc_html_e( 'Ever felt out of place?', UC_TEXT_DOMAIN ); ?></h1>
		</header>

		<div class="page-content container-xl">
			<?php
				
				$image = apply_filters( 'uc_404_image', '' );

				if( empty( $image ) ){

					// Get a random 404 image
					$mime_start	= 'image/';
					$theme_dir	= apply_filters( 'uc_404_image_dir', 'img/404' );
					$base_dir	= get_theme_file_path( $theme_dir );

					$dir_404	= new DirectoryIterator( $base_dir );
					$images		= array();
					
					// Take only files that have the appropriate MIME type
					foreach( $dir_404 as $item ):
						if( $item->isFile() && ( substr( mime_content_type( sprintf( '%s/%s', $base_dir, $item->getFileName() ) ), 0, strlen( $mime_start ) ) == $mime_start ) ):
							$images[] = sprintf( '%s/%s', get_theme_file_uri( $theme_dir ), $item->getFileName() );
						endif;
					endforeach;

					$image = $images[rand( 0, count( $images ) - 1 )];

				}
			?>

			<figure class="my-5">
				<img class="img-fluid 404-image" src="<?php echo $image ?>" title="404 decorative image">
			</figure>

			<p class="text-center"><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try searching?', UC_TEXT_DOMAIN ); ?></p>

			<?php ucdf_search_form(); ?>

		</div>
	</section>

<?php
get_footer();
