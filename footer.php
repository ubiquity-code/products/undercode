<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Ubiquity_Undercode
 */
?>
	</main>

	<?php get_template_part( 'inc/content', 'footer' ); ?>

	<?php // Fixed-bottom mobile buttons ?>
	<div class="fixed-bottom d-flex justify-content-around d-md-none pb-3 w-100">
		<a href="tel:01234" title="Call us" class="btn-footer btn-secondary "><?php ucdf_icon( 'phone', 'mr-1', 's' ) ?></a>
			
		<button type="button" role="button" class="btn-footer btn-primary controller" data-ucdf-controls="menu-offcanvas" data-ucdf-control-method="class" data-ucdf-control-class="d-none">
			<?php ucdf_icon( 'bars', 'fa-lg' ) ?>
		</button>

		<a href="mailto:info@orchardproperty.net?Subject=Enquiry" title="Email us" class="btn-footer btn-secondary"><?php ucdf_icon( 'envelope', 'mx-1', 's' ) ?></a>
	</div>

	<?php get_template_part( 'inc/footer', 'offcanvas' ); ?>

<?php ucdf_footer();
