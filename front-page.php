<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Ubiquity_Undercode
 */

get_header();

	while ( have_posts() ) :
		the_post();

		get_template_part( 'inc/content', 'page' ); ?>

		<div class="front-sections mt-5"><?php
			if( UC_FRONT_SECTIONS ){
				uc_page_children();			
			} ?>
		</div><?php

	endwhile; // End of the loop.

get_footer();
